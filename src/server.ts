import * as Koa from 'koa';
import * as Router from 'koa-router';

const app = new Koa();

app.use(async(ctx: any, next: any) => {
    console.log('Url:', ctx.url);
    await next();
});

const router = new Router();
router.get('/*', async(ctx: any) => {
    ctx.body = 'Hello, Koa!'
});

app.use(router.routes());
app.listen(3000);

console.log(`server running on port 3000`);